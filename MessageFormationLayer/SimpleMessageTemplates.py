


MessageTemplates={

    "simple":{
        "createMessageGreeting":{
            "basic":[
                "Namaste! I am Lakshmi, the travel expert from Asian Adventures."
            ],

            "namePresent": [
                "Thank you! Bye!",

            ],
            "askForUserForm": [
                "Awesome, please fill in your details and we will get back to you soonest.",

            ],

}
    }
}

